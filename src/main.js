import { createApp } from 'vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import bootstrap from 'bootstrap/dist/js/bootstrap.bundle.js'
// import router from '@/router/router'
import router from '@/main/'
import '@/assets/print.css'
import '@/assets/hidden.css'
import UserPage from "@/pages/UserPage.vue"
import PostIdPage from "@/pages/PostIdPage.vue"
import RegistrationPage from "@/pages/RegistrationPage.vue"
import {createRouter, createWebHashHistory} from "vue-router"

export default createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            component: UserPage
        },
        {
            path: '/registration',
            component: RegistrationPage,
        },
        {
            path: '/:id/:title/:body',
            component: PostIdPage,
        }
    ]
})

createApp(App).use(bootstrap).use(router).mount('#app')